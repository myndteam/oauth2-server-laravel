<?php
/**
 * Fluent adapter for an OAuth 2.0 Server
 *
 * @package   lucadegasperi/oauth2-server-laravel
 * @author    Luca Degasperi <luca@lucadegasperi.com>
 * @copyright Copyright (c) Luca Degasperi
 * @licence   http://mit-license.org/
 * @link      https://github.com/lucadegasperi/oauth2-server-laravel
 */

namespace LucaDegasperi\OAuth2Server\Storage;

use League\OAuth2\Server\Storage\Adapter;
use Illuminate\Database\ConnectionResolverInterface as Resolver;

abstract class FluentAdapter extends Adapter
{
    protected $accessTokenTableName;
    protected $accessTokenScopeTableName;
    protected $scopeTableName;
    protected $grantTableName;
    protected $grantScopeTableName;
    protected $clientTableName;
    protected $clientEndpointTableName;
    protected $clientScopeTableName;
    protected $clientGrantTableName;
    protected $sessionTableName;
    protected $sessionScopeTableName;
    protected $authCodeTableName;
    protected $authCodeScopeTableName;
    protected $refreshTokenTableName;

    /**
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected $resolver;

    /**
     * @var string
     */
    protected $connectionName;

    public function __construct(Resolver $resolver)
    {
        $prefix = env("OAUTH_TABLE_PREFIX", "luca_");
        $this->accessTokenTableName = "{$prefix}oauth_access_tokens";
        $this->accessTokenScopeTableName = "{$prefix}oauth_access_token_scopes";
        $this->scopeTableName = "{$prefix}oauth_scopes";
        $this->grantTableName = "{$prefix}oauth_grants";
        $this->grantScopeTableName = "{$prefix}oauth_grant_scopes";
        $this->clientTableName = "{$prefix}oauth_clients";
        $this->clientEndpointTableName = "{$prefix}oauth_client_endpoints";
        $this->clientScopeTableName = "{$prefix}oauth_client_scopes";
        $this->clientGrantTableName = "{$prefix}oauth_client_grants";
        $this->sessionTableName = "{$prefix}oauth_sessions";
        $this->sessionScopeTableName = "{$prefix}oauth_session_scopes";
        $this->authCodeTableName = "{$prefix}oauth_auth_codes";
        $this->authCodeScopeTableName = "{$prefix}oauth_auth_code_scopes";
        $this->refreshTokenTableName = "{$prefix}oauth_refresh_tokens";

        $this->resolver = $resolver;
        $this->connectionName = null;
    }

    public function setResolver(Resolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function getResolver()
    {
        return $this->resolver;
    }

    public function setConnectionName($connectionName)
    {
        $this->connectionName = $connectionName;
    }

    protected function getConnection()
    {
        return $this->resolver->connection($this->connectionName);
    }
} 
